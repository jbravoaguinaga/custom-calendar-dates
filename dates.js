var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Setiembre", "Octubre", "Noviembre", "Diciembre"];
var dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];
var currentDate = new Date();

$(function(){
    date = showCurrentDate();

    month = date.getMonth() + 1; //Current month of the year
    monthName = monthNames[month - 1]; //Current month name of the year
    year = date.getFullYear(); //Current full year YYYY

    console.log(month, year);

    daysList = getDaysList(month, year);
    showDays(daysList, monthName, year);
});

function showCurrentDate(){
    date = new Date(); //Date init without parameters

    return date;
}

function getDaysList(month, year){
    date = new Date(year, month - 1, 1); //New date but beginning with day 1
    result = [];

    while (date.getMonth() == month - 1) {
        result.push(dayNames[date.getDay()]+" "+date.getDate());
        date.setDate(date.getDate() + 1);
    }

    return result;
}

function showDays(daysList, monthName, year){
    $("h1").html(monthName+" de "+year);

    html = "";

    $.each(daysList, function(i, row){
        html += "<label>"+row+"</label><br/>";
    });

    $("#meses").html(html);
}

function showPrevFwdMonths(type){
    if (type == "prev") {
        currentDate.setMonth(currentDate.getMonth() - 1);
    }else{
        //console.log("ANTES: "+currentDate.getMonth()+" - "+currentDate.getFullYear()+" //// "+currentDate);
        currentDate.setMonth(currentDate.getMonth() + 1);
        //console.log("DSPS: "+currentDate.getMonth()+" - "+currentDate.getFullYear()+" //// "+currentDate);
    }

    month = currentDate.getMonth() + 1;
    monthName = monthNames[month - 1]; //Current month name of the year
    year = currentDate.getFullYear();

    console.log(month - 1, year, currentDate);

    daysList = getDaysList(month, year);
    showDays(daysList, monthName, year);
}

$("#anterior").on("click", function(){
    showPrevFwdMonths('prev');
});

$("#despues").on("click", function(){
    showPrevFwdMonths('fwd');
});